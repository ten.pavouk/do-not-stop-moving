#ifndef MENU_H
#define MENU_H

//#include <SDL2/SDL_mixer.h>
#include "common.h"

#define MENU_PLACEMENT_CENTER 0


typedef struct Menu {
    int index;
    int length;
    struct SDL_Texture *items[100];
    struct SDL_Texture *cursor;
    int x;
    int y;
    int cursor_w;
    //Mix_Music *move_sound;
    //Mix_Music *select_sound;
} Menu;


Menu *create_menu(struct SDL_Renderer *r);
void menu_add(Menu *menu, struct SDL_Renderer *r, const char *item);
void configure_menu(struct game_config *config, Menu *menu, int placement);

int menu_up(Menu *menu);
int menu_down(Menu *menu);
int  menu_selected(Menu *menu);

void draw_menu(struct SDL_Renderer *r, Menu *menu);
void destroy_menu(Menu *menu);

#endif
