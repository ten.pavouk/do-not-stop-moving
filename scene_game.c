#include "scene.h"

#include <SDL2/SDL.h>
#include <stdlib.h>
#include <time.h>
#include "pavouk.h"
#include "animated_sprite.h"


#define MAX_BUTTONS 200


static SDL_Texture *background;
static SDL_Texture *gameover_bg;

static SDL_Texture *test_text;
static SDL_Texture *sprites[6];

AnimatedSprite player;
AnimatedSprite player_atk;
AnimatedSprite player_hurt;
AnimatedSprite player_dead;
int mode = 0;


static SDL_Texture *buttons[4];

int score;

SDL_Texture *score_texture;
SDL_Texture *score_text;

int gameover;

int rows[2][MAX_BUTTONS];
float rows_x[2][MAX_BUTTONS];


void load_buttons_texture(SDL_Renderer *r) {
    buttons[0] = load_texture(r, "Resources/up.png");
    buttons[1] = load_texture(r, "Resources/right.png");
    buttons[2] = load_texture(r, "Resources/down.png");
    buttons[3] = load_texture(r, "Resources/left.png");
}

int get_random_button(int row) {
    int r = rand() % 2;
    if (row == 0 && r == 1) return 2;
    if (row == 1 && r == 0) return 3;
    return r;
}

static void scene_init(game_config *config) {
    gameover = FALSE;
    srand(time(NULL));
    background = load_texture(config->r, "Resources/game_scene_two_rows.png");
    gameover_bg = load_texture(config->r, "Resources/gameover.png");

    load_buttons_texture(config->r);
    
    score_texture = PAPI_TextureFromText(config->r, "Score:");

    mode = 0;
    test_text = load_texture(config->r, "Resources/nightborne/run.png");
    PAPI_SliceTexture(config->r, test_text, 80, 6, player.sprites);
    player.loop = TRUE;
    player.speed = 100;
    player.i = 0;

    PAPI_SliceTexture(config->r, load_texture(config->r, "Resources/nightborne/atk.png"), 80, 12, player_atk.sprites);
    player_atk.loop = FALSE;
    player_atk.speed = 60;
    player_atk.i = 0;

    PAPI_SliceTexture(config->r, load_texture(config->r, "Resources/nightborne/hurt.png"), 80, 5, player_hurt.sprites);
    player_hurt.loop = FALSE;
    player_hurt.speed = 100;
    player_hurt.i = 0;

    PAPI_SliceTexture(config->r, load_texture(config->r, "Resources/nightborne/dead.png"), 80, 24, player_dead.sprites);
    player_dead.loop = FALSE;
    player_dead.speed = 100;
    player_dead.i = 0;

    score = 0;

    for (int row = 0; row < 2; row++) {
        for (int i = 0; i < MAX_BUTTONS; i++) {
            int b = get_random_button(row);
            int r = rand() % 4;
            if (r < 1) {
                rows[row][i] = b;
                rows_x[row][i] = 640 + i * 68;
            } else {
                rows[row][i] = -1;
                rows_x[row][i] = -58;
            }
        }
    }
}

int is_gameover() {
    for (int row = 0; row < 2; row++) {
        for (int i = 0; i < MAX_BUTTONS; i++) {
            if (rows_x[row][i] > -58) return FALSE;
        }
    }

    return TRUE;
}

void dec_score() {
    score--;
    if (score < 0) {
        mode = 3;
        player_dead.i = 0;
        gameover = TRUE;
        score = 0;
        return;
    }
    mode = 2;
    player_hurt.i = 0;
}

void update_btns(int deltatime) {
    float speed = deltatime / 10.0f;
    for (int row = 0; row < 2; row++) {
        for (int i = 0; i < MAX_BUTTONS; i++) {
            if (rows[row][i] > -1) {
                rows_x[row][i] -= speed;

                if (rows_x[row][i] < 25) {
                    dec_score();
                    rows[row][i] = -1;
                }
            }
        }
    }
}

int get_hittable_button(int row) {
    for (int i = 0; i < MAX_BUTTONS; i++) {
        if (rows[row][i] > -1 && rows_x[row][i] < 81 && rows_x[row][i] > 25)
            return i;
    }

    return -1;
}

void handle_row(keys_state keys, int row) {
    int up_pressed = keys.pressed_keys[UP];
    int right_pressed = keys.pressed_keys[RIGHT];
    int down_pressed = keys.pressed_keys[DOWN];
    int left_pressed = keys.pressed_keys[LEFT];

    int i = get_hittable_button(row);
    if (
        i > -1 &&
        (
            (rows[row][i] == 0 && up_pressed && !down_pressed) ||
            (rows[row][i] == 2 && down_pressed && !up_pressed) ||
            (rows[row][i] == 1 && right_pressed && !left_pressed) ||
            (rows[row][i] == 3 && left_pressed && !right_pressed)
        )
    )
    {
        score++;
        rows_x[row][i] = -58;
        rows[row][i] = -1;
        mode = 1;
        player_atk.i = 0;
    }
}

void _update_player(int deltatime) {
    switch (mode) {
        case 0:
            update_sprites(&player, deltatime);
            break;
        case 1:
            update_sprites(&player_atk, deltatime);
            if (!player_atk.sprites[player_atk.i + 1]) {
                mode = 0;
                player.i = 0;
            }
            break;
        case 2:
            update_sprites(&player_hurt, deltatime);
            if (!player_hurt.sprites[player_hurt.i + 1]) {
                mode = 0;
                player.i = 0;
            }
            break;
        case 3:
            update_sprites(&player_dead, deltatime);
            break;
        default:
            break;
    }
}

static void scene_update(game *game, int deltatime) {
    keys_state keys = game->config.keys_state;
    if (keys.pressed_keys[CANCEL])
        game->manager.change(game, 0);

    _update_player(deltatime);
    if (is_gameover() || gameover) {
        if (keys.pressed_keys[ACTION]) {
            game->manager.change(game, 0);
        }
        return;
    }

    //SDL_Log("%d\n", deltatime);
    update_btns(deltatime);
    handle_row(keys, 0);
    handle_row(keys, 1);
}

void show_score(SDL_Renderer *r) {
    char x[7];
    snprintf(x, 7, "%d", score);

    score_text = PAPI_TextureFromText(r, x);
    draw_texture(r, score_texture, 500, 30);
    draw_texture(r, score_text   , 600, 30);
}

void _draw_player(SDL_Renderer *r) {
    switch (mode) {
        case 0:
            draw_texture(r, player.sprites[player.i], 280, 340);
            break;
        case 1:
            draw_texture(r, player_atk.sprites[player_atk.i], 280, 340);
            break;
        case 2:
            draw_texture(r, player_hurt.sprites[player_hurt.i], 280, 340);
            break;
        case 3:
            draw_texture(r, player_dead.sprites[player_dead.i], 280, 340);
            break;
        default:
            break;
    }
}

static void scene_render(SDL_Renderer *r) {
    draw_texture(r, background, 0, 0);

    _draw_player(r);
    show_score(r);
    if (is_gameover() || gameover) {
        draw_texture(r, gameover_bg, 0, 0);
        return;
    }

    for (int row = 0; row < 2; row++) {
        for (int i = 0; i < MAX_BUTTONS; i++) {
            if (rows_x[row][i] > -58)
                draw_texture(r, buttons[rows[row][i]], rows_x[row][i], 177 + row * 68);
            }
    }
}

static void scene_exit() {
    SDL_DestroyTexture(background);
}


game_scene scene_game = {
    scene_init,
    scene_update,
    scene_render,
    scene_exit
};

