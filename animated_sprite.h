#ifndef ANIMATED_SPRITE_H
#define ANIMATED_SPRITE_H

#include "common.h"


typedef struct AnimatedSprite {
    struct SDL_Texture *sprites[50];
    int i;
    int timer;
    int loop;
    int speed;
} AnimatedSprite;


void update_sprites(AnimatedSprite *sprite, int deltatime);

#endif
