#ifndef AUDIO_MANAGER_H
#define AUDIO_MANAGER_H


typedef struct AudioManager {
    struct Mix_Chunk *music;
} AudioManager;


void destroy_audio_manager(AudioManager *audio);
static AudioManager audio_manager;

#endif
