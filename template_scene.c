#include "scene.h"

#include <SDL2/SDL.h>
#include "pavouk.h"


static SDL_Texture *background;

static void scene_init(game_config *config) {
    background = load_texture(config->r, "Resources/background.png");
}

static void scene_update(game *game, int deltatime) {
    keys_state keys = game->config.keys_state;
    if (keys.pressed_keys[ACTION]) {
    }
}

static void scene_render(SDL_Renderer *r) {
    draw_texture(r, background, 0, 0);
}

static void scene_exit() {
    SDL_DestroyTexture(background);
}


game_scene scene_ = {
    scene_init,
    scene_update,
    scene_render,
    scene_exit
};

