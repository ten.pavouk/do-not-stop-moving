# Attacking Rhythm

Game developed for a game jam during one weekend: [artists-tavern-game-jam](https://itch.io/jam/artists-tavern-game-jam)

## Technologies Used

- C Language
- CMake
- [ChipTone](https://sfbgames.itch.io/chiptone)

## Developed by:

Ten Pavouk - **ten.pavouk@proton.me**

## Music by:

[xdeviruchi](https://xdeviruchi.itch.io/8-bit-fantasy-adventure-music-pack)

## Art by:

[creativekind](https://creativekind.itch.io/nightborne-warrior)

