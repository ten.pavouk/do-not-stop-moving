#include "animated_sprite.h"


void update_sprites(AnimatedSprite *sprite, int deltatime) {
    sprite->timer += deltatime;
    if (sprite->timer >= sprite->speed) {
        sprite->timer -= sprite->speed;
        if (sprite->sprites[sprite->i + 1]) {
            sprite->i++;
        } else {
            if (sprite->loop) sprite->i = 0;
        }
    }
}

