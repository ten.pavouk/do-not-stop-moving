#include "menu.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <stdlib.h>
#include "pavouk.h"


Menu *create_menu(SDL_Renderer *r) {
    Menu *menu = malloc(sizeof(Menu));
    menu->index = -1;
    menu->length = 0;
    menu->cursor = load_texture(r, "Resources/cursor.png");
    menu->x = 0;
    menu->y = 0;

    SDL_QueryTexture(menu->cursor, NULL, NULL, &menu->cursor_w, NULL);

    //menu->select_sound = Mix_LoadMUS("Resources/coin.ogg");
    //menu->move_sound = Mix_LoadMUS("Resources/coin.ogg");
    return menu;
}

void menu_add(Menu *menu, SDL_Renderer *r, const char *item) {
    menu->index = 0;
    menu->items[menu->length] = PAPI_TextureFromText(r, item);
    menu->length++;
}

int _get_max_width(Menu *menu) {
    int max = 0;
    for (int i = 0; i < menu->length; i++) {
        int w;
        SDL_QueryTexture(menu->items[i], NULL, NULL, &w, NULL);
        if (w > max) max = w;
    }
    return max;
}

void _centralize_menu(game_config *config, Menu *menu) {
    int w = _get_max_width(menu);
    menu->x = (config->width - w) / 2;

    int item_h;
    SDL_QueryTexture(menu->items[0], NULL, NULL, NULL, &item_h);
    int h = item_h * menu->length + (menu->length - 1) * 32;
    menu->y = (config->height - h) / 2;
}

void configure_menu(game_config *config, Menu *menu, int placement) {
    switch (placement) {
        case MENU_PLACEMENT_CENTER:
            _centralize_menu(config, menu);
            break;
        default:
            break;
    }
}

int menu_up(Menu *menu) {
    if (--menu->index < 0) {
        menu->index = 0;
        return FALSE;
    }
    return TRUE;
    //else Mix_PlayMusic(menu->move_sound, 1);
}

int menu_down(Menu *menu) {
    if (++menu->index >= menu->length) {
        menu->index = menu->length - 1;
        return FALSE;
    }
    return TRUE;
    //else Mix_PlayMusic(menu->move_sound, 1);
}

int menu_selected(Menu *menu) {
    //Mix_PlayMusic(menu->select_sound, 1);
    return menu->index;
}

void draw_menu(SDL_Renderer *r, Menu *menu) {
    for (int i = 0; i < menu->length; i++) {
        draw_texture(r, menu->items[i], menu->x, menu->y + 32 * i);
    }
    draw_texture(r, menu->cursor, menu->x - menu->cursor_w, menu->y + 32 * menu->index);
}

void destroy_menu(Menu *menu) {
    for (int i = 0; i < menu->length; i++) {
        if (menu->items[i]) SDL_DestroyTexture(menu->items[i]);
    }

    //Mix_FreeMusic(menu->move_sound);
    //Mix_FreeMusic(menu->select_sound);
    SDL_DestroyTexture(menu->cursor);
    free(menu);
}

