#include <SDL2/SDL.h>
#include "pavouk.h"
#include "scene.h"
#include "audio_manager.h"


void create_game(game_config *config) {
    config->width = 640;
    config->height = 480;
    config->title = "Attacking Rhythm";
}

void create_scenes(scene_manager *manager) {
    manager->add_scene(manager, scene_title);
    manager->add_scene(manager, scene_credits);
    manager->add_scene(manager, scene_game);
    manager->current_scene = 0;
}

void exit_game(game_config *config) {
    destroy_audio_manager(&audio_manager);
}
