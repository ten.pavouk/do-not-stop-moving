#include "audio_manager.h"

#include <SDL2/SDL_mixer.h>


void destroy_audio_manager(AudioManager *audio) {
    Mix_FreeChunk(audio->music);
}

