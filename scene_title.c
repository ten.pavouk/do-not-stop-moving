#include "scene.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include "pavouk.h"
#include "menu.h"
#include "audio_manager.h"


static SDL_Texture *background;
static Menu *menu;


static void scene_init(game_config *config) {
    background = load_texture(config->r, "Resources/scene_title.png");
    menu = create_menu(config->r);
    menu_add(menu, config->r, "New Game");
    menu_add(menu, config->r, "Credits");
    menu_add(menu, config->r, "Quit");
    configure_menu(config, menu, MENU_PLACEMENT_CENTER);
    menu->y += 60;

    audio_manager.music = Mix_LoadWAV("Resources/music.wav");
    if (!Mix_Playing(0)) {
        Mix_PlayChannel(0, audio_manager.music, -1);
    }
    Mix_Volume(0, 64);
}

static void scene_update(game *game, int deltatime) {
    keys_state keys = game->config.keys_state;
    if (keys.pressed_keys[UP]) {
        if (menu_up(menu)) {}
    } else if (keys.pressed_keys[DOWN]) {
        if (menu_down(menu)) {}
    } else if (keys.pressed_keys[ACTION]) {
        switch (menu_selected(menu)) {
            case 0:
                game->manager.change(game, 2);
                break;
            case 1:
                game->manager.change(game, 1);
                break;
            case 2:
                game->config.is_running = FALSE;
                break;
            default:
                break;
        }
    }
}

static void scene_render(SDL_Renderer *r) {
    draw_texture(r, background, 0, 0);
    draw_menu(r, menu);
}

static void scene_exit() {
    SDL_DestroyTexture(background);
    destroy_menu(menu);
}


game_scene scene_title = {
    scene_init,
    scene_update,
    scene_render,
    scene_exit
};

